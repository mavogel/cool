#!/usr/bin/env python3
def build_gitlabci(binary_tag, view, OS):
  name = f"build_{binary_tag}_{view}"
  return f"""{name}:
  variables:
    BINARY_TAG: {binary_tag}
  artifacts: 
    when: always
    expire_in: "3 mos"
    paths: 
      - logs/qmtestCool/$BINARY_TAG
  before_script:
    - "echo $BINARY_TAG $CI_COMMIT_REF_NAME"
    - "export highest=$(curl --header \\\"PRIVATE-TOKEN: $PRIVATE_TOKEN\\\" \\\"https://gitlab.cern.ch/api/v4/projects/25684/jobs?per_page=100\\\" | jq  \\\".[] | select(.name == \\\\\\\"{name}\\\\\\\") | select(.ref == \\\\\\\"${{CI_COMMIT_REF_NAME}}\\\\\\\") | .id\\\" | sort -n | tail -1)"
    - "echo $highest"
    - "curl -L --header \\\"PRIVATE-TOKEN: $PRIVATE_TOKEN\\\" \\\"https://gitlab.cern.ch/api/v4/projects/25684/jobs/$highest/artifacts\\\" > coral-{name}.zip"
    - "ls -l coral-{name}.zip"
    - "unzip coral-{name}.zip -d ./coral"
  script: 
    - "./cc-build -b $BINARY_TAG -v {view}/latest"
    - cd $BINARY_TAG/
    - ./cc-sh
    - "export CORAL_AUTH_PATH=/home/gitlab-runner/"
    - "export CORAL_DBLOOKUP_PATH=/home/gitlab-runner/"
    - "export COOL_QMTEST_USER=sftnight"
    - "time ./qmtestRun.sh"
    - ../logs/qmtestCool/summarizeAll.sh
    - "cd ../logs/qmtestCool/"
    - "mkdir $BINARY_TAG"
    - "cp $BINARY_TAG.summary $BINARY_TAG.xml ./$BINARY_TAG"
    - "! grep -e FAIL -e ERROR $BINARY_TAG.summary"
  tags: 
    - {OS}

"""

platforms=['dev3-x86_64-centos7-clang10-dbg', 'dev3-x86_64-centos7-clang10-opt', 'dev3-x86_64-centos7-gcc10-dbg', 'dev3-x86_64-centos7-gcc10-opt', 'dev3-x86_64-centos7-gcc8-dbg', 'dev3-x86_64-centos7-gcc8-opt', 'dev3-x86_64-centos7-gcc9-dbg', 'dev3-x86_64-centos7-gcc9-opt', 'dev3python2-x86_64-centos7-gcc8-opt', 'dev3python2-x86_64-centos7-gcc9-opt']
 
with open('.gitlab-ci.yml', 'w') as f:
  f.write("""---
""")
  for platform in platforms:
    list = platform.split('-')
    f.write(build_gitlabci(platform[platform.find('-')+1:], list[0], list[2]))

