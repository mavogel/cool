#!/usr/bin/env python
import sys

# import C++ <-> Python interface: 
# - cppyy exists in ROOT6 and also in ROOT >= 5.34.11, but not in <= 5.34.10
# - PyCintex exists in ROOT5, but not in ROOT6
# NB use cppyy and drop any use of PyCintex (drop PyCool support for ROOT5!)
# NB use cppyy also in definitions.py and all files previously using PyCintex
# NB PyCintex was used instead of cppyy in the past also to fix CORALCOOL-2731
try:
    import cppyy
except:
    raise RuntimeError("ERROR! cppyy not found!")

# use gROOT.GetVersion() to distinguish between ROOT5/6 (CORALCOOL-2732)
# [eventually move to cppyy.get_version() if cppyy.gbl.gROOT is removed]
if cppyy.gbl.gROOT.GetVersion()[0] == '5' :
    raise RuntimeError("ERROR! ROOT5 is no longer supported in PyCool!")
else:
    # this is ROOT6: manually load COOL libraries and headers
    cppyy.gbl.gSystem.Load('liblcg_CoolApplication.so')
    import os
    include_line = '#include "%s"' % os.path.join(os.path.dirname(__file__), '_internal', 'PyCool_headers_and_helpers.h')
    if not cppyy.gbl.gInterpreter.Declare(include_line): # see ROOT-5698
        raise RuntimeError("ERROR! Could not include PyCool headers")

# expose only few symbols when loaded with "import *"
__all__ = [ 'coral', 'cool', 'walk' ]

# expose namespaces
cool  = cppyy.gbl.cool
coral = cppyy.gbl.coral

# special operations
from ._internal import instrumentation
instrumentation.all()

# import platform utilities
from ._internal.utilities import is_64bits, getRootVersion

# delete temporary names that should not be seen by users
del instrumentation
del _internal
del sys

class walk:
    """
    Iterator to walk through the hierarchy of a COOL database.
    It mimics the function os.walk, but the navigation cannot
    be modified changing in place the returned directories.

    Example:
        for root, foldersets, folders in PyCool.walk(db,'/'):
            ...
    """
    def __init__(self,db,top="/"):
        """
        Construct a new iterator instance.
        db has to be a COOL database instance, and top is the FolderSet
        path where to start the navigation.
        """
        self.db = db
        self.dirs = [ top ]
    def __iter__(self):
        return self
    def next(self):
        if self.dirs:
            # get next folderset to process
            root = self.dirs.pop()
            fs = self.db.getFolderSet(root)
            # get list of contained foldersets in reversed alfabetical order
            dirs = [ f for f in fs.listFolderSets(False) ]
            # prepend the found foldersets to the list of foldersets to process
            self.dirs = dirs + self.dirs

            # get the legnth of the part of the name to strip
            if root == '/':
                to_remove = 1
            else:
                to_remove = len(root)+1
            # sort alfabetically the foldersets and leave the FolderSet name
            dirs.sort()
            dirs = [  d[to_remove:] for d in dirs ]
            # get the list of folders and leave only the Folder name
            files = [ f[to_remove:] for f in fs.listFolders() ]
            return (root, dirs, files)
        else:
            # we do not have any other folderset to process
            raise StopIteration()
