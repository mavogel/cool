# See also test_root6068b.py (and CORALCOOL-2959)
import sys
if len(sys.argv)!=2 or ( sys.argv[1]!='nocrash' and sys.argv[1]!='crash1' and sys.argv[1]!='crash2' ):
    # NB: 'crash1' and 'crash2' cause a crash, 'nocrash' does not...
    print "Usage: python", sys.argv[0], "<nocrash|crash1|crash2>"
    sys.exit(0)
crash1=False
crash2=False
if sys.argv[1]=='crash1': crash1=True
elif sys.argv[1]=='crash2': crash2=True
print "Your choice: CRASH1 (do not add a printout) =",crash1
print "Your choice: CRASH2 (do not add a template) =",crash2

# Build and import a test library
import os
dir=os.path.dirname( os.path.realpath(__file__) )
os.system("cd "+dir+"; $CXX -std=c++1y -pedantic -Wall -Wextra -Wno-long-long -Wno-deprecated -pthread -fPIC -Wl,--no-undefined -g -fPIC -o test_root8458g_lib.o -c test_root8458g_lib.cpp; $CXX -fPIC -std=c++1y -pedantic -Wall -Wextra -Wno-long-long -Wno-deprecated -pthread -fPIC -Wl,--no-undefined -g   -shared -Wl,-soname,test_root8458g_lib.so -o test_root8458g_lib.so test_root8458g_lib.o")
import cppyy
cppyy.gbl.gSystem.Load(dir+"/test_root8458g_lib.so")
if crash1: cppyy.gbl.gInterpreter.ProcessLine("#define CRASH1 1")
if crash2: cppyy.gbl.gInterpreter.ProcessLine("#define CRASH2 1")
cppyy.gbl.gInterpreter.ProcessLine('#include "'+dir+'/test_root8458g.h"')
rbug  = cppyy.gbl.rbug

# Test (as in test_root6068.py, unstable vs crash/nocrash)
rbug.FieldSelection4("i",rbug.StorageType4.Int32,rbug.FieldSelection4.EQ,10)

# Test (as in the original test_root8458.py, no instability observed)
#rbug.FieldSelection4("x",rbug.StorageType4.Bool,rbug.FieldSelection4.EQ,True)
#rbug.FieldSelection4("x",rbug.StorageType4.Bool,rbug.FieldSelection4.EQ,10)
#rbug.FieldSelection4("x",rbug.StorageType4.Bool,rbug.FieldSelection4.EQ,10.)
