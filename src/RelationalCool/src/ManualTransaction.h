#ifndef RELATIONALCOOL_MANUALTRANSACTION_H
#define RELATIONALCOOL_MANUALTRANSACTION_H 1

#include "CoolKernel/VersionInfo.h" // for #ifdef COOL4xx 

// This class only exists in the COOL400 API
#ifdef COOL400TX

// Include files
#include <memory>
#include "CoolKernel/ITransaction.h"

namespace cool
{

  // Forward declaration
  class IRelationalTransactionMgr;

  class ManualTransaction : public ITransaction
  {

  public:

    /// Destructor
    virtual ~ManualTransaction();

    /// Constructor from a IRelationalTransactionMgr
    ManualTransaction
    ( const std::shared_ptr<IRelationalTransactionMgr>& transactionMgr,
      bool readOnly = false );

    /// Commit the transaction *and* re-enable auto-transaction mode
    void commit();

    /// Rollback the transaction *and* re-enable auto-transaction mode
    void rollback();

  private:

    /// Standard constructor is private
    ManualTransaction();

    /// Copy constructor is private
    ManualTransaction( const ManualTransaction& rhs );

    /// Assignment operator is private
    ManualTransaction& operator=( const ManualTransaction& rhs );

  private:

    /// Handle to the IRelationalTransactionMgr (shared ownership)
    std::shared_ptr<IRelationalTransactionMgr> m_transactionMgr;

  };

} // namespace

#endif

#endif
