Dec 2004

* Tablespaces
- Set default data and index tablespaces for all data

Dec 2004

* Folder sets, folders, HVS nodes
- Main/Only priority: method to list children of a specific folder set,
  possibly with recursion, possibly distibguish folder and folder sets within
- Add various list methods to HvsNode, inherited by folderset:
  listChildren, listLeafChildren, listBranchChildren.
  Or have a flag to show only leaves/branches?
  Flag for recursive listing (or # nested directories to show)?
- Use protected inheritance for RelationalFolder : HvsNode?
  For instance can be useful to just rename listLeafChildren 
  as listFolders...
- Add method getRootFolderSet() to IDatabase: note that listFolders
  can then be delegated to this alone
- I would not delegate create/get folder to folder sets though:
  use only full paths, no relative paths? Or allow relative paths
  to make renaming eventually easier?
- CreateFolder and createFolderSet have a lot in common,
  can be factored out
- Better definition of Folder and FolderSet classes: need a separate
  IFolderSet or use IFolder to indicate both? I would definitely
  use two separate classes, folder handles IOVs, while folderset
  handles folders within itself.

------------------------------------------------------------------------------
Nov 2004

* DatabaseId
- Remove, replace by string in the createDatabase signature

* RalDatabaseId
- Move to public API of RelationalCool?
- Add constructor from parameters?
- Add url() method to return a string?
- Add automatic conversion to a string 
  so that it can be passed to createDatabase?

* Relational vs Ral or MySQL
- Prefix "Relational" for classes that may be both Ral or MySQL?
- Four levels of inheritance (IDatabase, Database, RelationalDatabase,
  RalDatabase/MySQLDatabase)?
  --> sas: why Database?

* Group typedefs in types.h?
+ Or rather keep in separate files? eg XXXPtr in XXX.h
  --> added types.h. Not intended to be manually included by API clients
      (documented accordingly)

* Define ValidityKey as seal::LongLong typedef
+ sas: done.

* Add schema version to the management table

* IOV table schema
- system-inserted IOV id
+ done. channelID, since, until, payload
- system-inserted insertion time

* Define base Exception class in CoolKernel
+ sas: done. derived from seal::Exception
+ sas: done. derive RelationalException from cool::Exception

* Clean up the hierarchy and loading of libraries
- RAL hierarchy
  0. Top: IRelationalService -> one singleton (in RelationalAccess) 
       "POOL/Services/RelationalService" - RelationalService
    1. IRelationalDomain -> three singletons (in three packages)
         "POOL/RelationalPlugins/mysql/odbc" - MySqlODBCDomain
         "POOL/RelationalPlugins/oracle" - OracleDomain
         "POOL/RelationalPlugins/sqlite" - SQLiteDomain        
     2. IRelationalSession -> one instance per open session
- in RAL a string only specifies where the data is (different drivers
  can exist for MySQL, but the data is always the same, a table with
  a given name in MySQL)... for COOL, different implementations may 
  imply completely different schemas for the data, it is not the same
  ==> we should make the names specify the plugin (as Sven was saying)
  ==> one possibility: use "ral::oracle::", "ral::mysql::", "mysql::"... 
      (in this example, ral and mysql would be two plugins)
- for comparison: RAL technology-dependent switch is in RelationalService.cpp
  (technology name is directly translated to plugin name by adding a suffix,
   e.g. 'oracle' -> IRelationalDomain 'POOL/RelationalPlugins/oracle' )

* PROPOSAL
  0. Top: IDatabaseService -> one singleton (in CoolKernel or CoolBase) 
       "COOL/Services/DatabaseService" - DatabaseService
    1. IDatabaseService -> one singleton per implementation
         "COOL/KernelServicePlugins/ral" - RalDatabaseService
         "COOL/KernelServicePlugins/mysql" - MySQLDatabaseService
     2. IDatabase -> one instance per open session (i.e. per open database)

* Centralise all SQL implementation in the database, nothing in the folders
- Session is only needed in the database

* Replace IDatabase boost pointers by references
- Place ownership of the instances in the RalDatabaseSvc

* Beware of catch 22 in schema upgrades (sas, 2004-12-10)
- I've just encountered the following situation:
In preparation of the foldersets, the folder table schema changed. Now, when 
dropDatabase executes, it uses the folder table to get the object table names.
Due to the schema change, this access failed and the dropDatabase chokes and
returns without dropping any tables. Admittedly, this is a very special case
typically encountered during the early development stages, but we've seen the 
problem of dropDatabase not working successfully because it could not delete a
table before. It would be useful in general to make dropDatabase a bit more
robust, i.e. not dependent on obtaining all the management information to
do its job.

I think dropDatabase should at least assert that the management tables are
dropped, so that a subsequent createDatabase is successful(*). It should report 
if it cannot delete the object tables (e.g. due to a read failure in the folder
table) to make the user aware that there are data tables left.

Folder creation then has to be looked at to decide what to do if a new object
table name clashes with an existing one. The options are: drop and recreate
the table or try with the next ID in the sequence. I think we should aim for
option 2.

(*) This is what the drop database test currently checks for.


* The seal configuration file env variable does not seem to work. The msg
output level is only taken from the file it is in the current working
directory.


* [sas 2004-12-13: I wrote the following on the train before finding out that 
Andrea had impemented option #2]
sas, 2004-12-10:
Rollback warning. Due do our transaction class doing an automatic rollback
in the destructor, we now get a warning from RAL each time a RalTransaction
goes out of scope:
  POOL/RelationalPlugins/oracle  Warning No active transaction to roll back
[edit: We still do get some warnings even though Andrea's changes make sure 
it's not from our Transaction destructor when there has been a commit. We 
get one commit warning per test, so it seems this occurs when the session
object goes out of scope (or something along that line).]
  
Even if it doesn't incur a performance penalty it does not look pretty.
Perhaps we should add the following changes:

RalTransaction::commit() {
  m_session->commit();
  m_autocommit = true;
}

RalTransaction::~RalTransaction() {
  if ( m_autocommit ) {
    m_session->commit();
  } else {
    m_session->rollback();
}

or

RalTransaction::commit() {
  m_session->commit();
  m_closed = true;
}

RalTransaction::~RalTransaction() {
  if ( ! m_closed ) {
    m_session->rollback();
}

The second option has the drawback that we lose the guarantee of not leaving
open transactions, which was the primary purpose of the class. I would go
with the first option, where the RalTransaction is practically switched to
autocommit mode after the first commit by the user.

Basically the class is intended for single commits, you instantiate it 
once, do a commit once and scope it in a way that there's no other SQL handled 
by the transaction anyway. It still does provide the safeguard of rollback 
should an exception occur before the transaction.commit().



